﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApp20
{
    class Program
    {

        static string failInimene = @"..\..\Data\Inimesed.txt";
        static void Main(string[] args)
        {
            LoeInimesed();

            string küsimus = "";
            do
            {
                Console.Write("mida teha tahad\nList - loetelu, Lisa - uus inimene, Exit - lõpeta: ");
                küsimus = Console.ReadLine().ToLower();
                switch (küsimus)
                {
                    case "list":

                        foreach (var x in Inimene.Inimesed)
                        {
                            Console.WriteLine(x);
                        }

                        break;
                    case "lisa":

                        Console.Write("Anna nimi: ");
                        string nimi = Console.ReadLine();
                        Console.Write("Anna vanus: ");
                        int vanus = int.Parse(Console.ReadLine());
                        Inimene.Inimesed.Add(new Inimene() { Nimi = nimi, Vanus = vanus });
                        break;
                    case "exit":
                    case "":
                        Console.WriteLine("no ma siis lõpetan");
                        break;
                    default:
                        break;

                }

            }
            while (küsimus != "" && küsimus != "exit");

            SalvestaInimesed();
        }

        static void LoeInimesed()
        {
            string[] loetudRead = File.ReadAllLines(failInimene);
            foreach (var r in loetudRead)
                Inimene.Inimesed.Add(new Inimene() { Nimi = r.Split(' ')[0], Vanus = int.Parse(r.Split(' ')[1]) });
        }

        static void SalvestaInimesed()
        {
            File.WriteAllLines(failInimene,
            Inimene.Inimesed.Select(x => x.ToString()).ToArray()
            );
        }
    }

    class Inimene
    {
        public static List<Inimene> Inimesed = new List<Inimene>();
        public string Nimi;
        public int Vanus;

        public override string ToString()
        {
            return $"{Nimi} {Vanus}";
        }
    }
}
